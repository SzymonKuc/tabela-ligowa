import React, { Component } from 'react';
import DrawTable from './components/tbody';
import AddScore from './components/inputs';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import './App.css';
import _ from 'lodash'

/*
  Komponenty:
  - tworzenie tabeli (tbody.jsx)
  - wprowadzanie wynikow (inputs.jsx)

  Tabela - miejsce, nazwa, ilosc spotkan, zwyciestwa, remisy, porazki, bramki strzelone, bramki stracone, punkty

  data = {position, name, matchCount, wins, draws, losts, scoredGoals, lostGoals, points}

*/

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      team1Goals: '',
      team2Goals: '',
      team1: '',
      team2: '',
    };
  }

  addInputs = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  loadJSON = (files) => {
    if (files.name.split('.').pop() === 'json') {
      const reader = new FileReader();
      let jsonFile;
      reader.onloadend = () => {
        jsonFile = reader.result;
        jsonFile = JSON.parse(jsonFile);
        this.setState({ data: jsonFile.League.Team });
        this.addScoresToData();
      };
      reader.readAsText(files);
    } else {
      alert('Wprowadz plik z rozszerzeniem .json!');
    }
  }
  
  addScoresToData = () => {

    let newData = [...this.state.data];

    newData.forEach((element) => { 
      for (const prop in element) {
        if (prop !== 'Name') {
          element[prop] = parseInt(element[prop]);
        }
      }
    });
    //Add points
    newData.forEach(team => team.Points = team.Wins * 3 + team.Draws);
    //Sort
    newData = _.orderBy(newData, ['Points','Wins', 'LostGoals'], ['desc', 'desc', 'asc']);
    //add position in table
    newData.forEach((team, index) => team.Position = index + 1);

    this.setState({ data: newData });
  }

  addNewScores = () => {

    let { team1Goals, team2Goals, team1, team2, data } = this.state;

    team1Goals = parseInt(team1Goals);
    team2Goals = parseInt(team2Goals);

    if(!isNaN(team1Goals) && !isNaN(team2Goals)){
      if(team1 !== '' && team2 !== ''){
        const newData = [...this.state.data];

        const [t1, t2] = [team1, team2].map((team) => {
          const found = data.find(({ Position }) => parseInt(team) === Position);
          if (found) {
            return found;
          }
          else{
              return null;
          }
          });

        //update scores
        t1.MatchCount += 1;
        t2.MatchCount += 1;
        t1.ScoredGoals += team1Goals;
        t2.ScoredGoals += team2Goals;
        t1.LostGoals += team2Goals;
        t2.LostGoals += team1Goals;
        //add win or lost or draw
        if (team1Goals > team2Goals) {
          t1.Wins += 1;
          t2.Losts += 1;
        } else if (team1Goals === team2Goals) {
          t1.Draws += 1;
          t2.Draws += 1;
        } else {
          t2.Wins += 1;
          t1.Losts += 1;
        }
        //add update teams to array
        newData.forEach((team) => {
          if (team.Name === t1.Name) {
            team = t1;
          } else if (team.Name === t2.Name) {
            team = t2;
          }
        })

        this.setState({ data: newData, team1: '', team2: '', team1Goals: '', team2Goals: '' });
 
        this.addScoresToData();
      }else{
        alert("Wybierz drużyne!");
      }
       
    }else{
      alert("Wprwadź liczbę!")
    }
    
  }

  render() {
    return (
      <CssBaseline>
        <div className="App">
          <h1>Tabela Ligowa</h1>
          <Button variant="contained" color="primary" onClick={this.handleClick}>Wybierz Plik</Button>
          <input type="file" id="file" name="files" accept=".json" onChange={e => this.loadJSON(e.target.files[0])} ref={input => this.inputElement = input}/>
          <DrawTable teamsData={this.state.data} />
          <AddScore teamsData={this.state} addInputs={this.addInputs} addNewScores={this.addNewScores} />
        </div>
      </CssBaseline>
    );
  }
  handleClick = () => {
    this.inputElement.click();
  }
}

export { App };

