import React from 'react';
import TextField from '@material-ui/core/TextField';
import '../App.css'
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';

const AddScore = props => {
    const { data, team1Goals, team2Goals, team1, team2 } = props.teamsData;
    
    if(props.teamsData.data.length > 0){

        const options = data.map((el, index) => {
            return(
                <MenuItem value={index + 1} key={index + 1}>{el.Name}</MenuItem>
            );
        });

        return (
            <div className="inputsContainer">
                
                <Select className="select" value={team1} name="team1" onChange={props.addInputs} displayEmpty>
                    <MenuItem value="" disabled>Wybierz drużyne</MenuItem>
                    {options} 
                </Select>
                <TextField id="outlined-number" variant="outlined" name="team1Goals" type="number" value={team1Goals} onChange={props.addInputs} /> <TextField id="outlined-number" variant="outlined" name="team2Goals" type="number" value={team2Goals} onChange={props.addInputs}/>
                <Select className="select" value={team2} name="team2" onChange={props.addInputs} displayEmpty>
                    <MenuItem value="" disabled>Wybierz drużyne</MenuItem>
                    {options} 
                </Select>
                
                <Button variant="contained" color="primary" onClick={props.addNewScores}>Zatwierdź</Button>

            </div>
    
        );
    }else{
        return null;
    }
}

export default AddScore;