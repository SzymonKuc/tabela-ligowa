import React from "react";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
const DrawTable = props => {

    const rows = props.teamsData.map((el, index) => {
        index += 1;
            return(
                <TableBody key={index}>
                <TableRow >
                    <TableCell>{index}</TableCell>
                    <TableCell>{el.Name}</TableCell>
                    <TableCell>{el.MatchCount}</TableCell>
                    <TableCell>{el.Wins}</TableCell>
                    <TableCell>{el.Draws}</TableCell>
                    <TableCell>{el.Losts}</TableCell>
                    <TableCell>{el.ScoredGoals}</TableCell>
                    <TableCell>{el.LostGoals}</TableCell>
                    <TableCell>{el.Points}</TableCell>
                    <TableCell></TableCell>
                    </TableRow> 
                </TableBody>
                );
            });
        if(props.teamsData.length > 0){
            return (
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Miejsce</TableCell>
                            <TableCell>Nazwa drużyny</TableCell>
                            <TableCell>Ilość spotkań</TableCell>
                            <TableCell>Zwycięstwa</TableCell>
                            <TableCell>Remisy</TableCell>
                            <TableCell>Porażki</TableCell>
                            <TableCell>B. strzelone</TableCell>
                            <TableCell>B. stracone</TableCell>
                            <TableCell>Punkty</TableCell>
                        </TableRow>
                    </TableHead>
                    {rows}
                </Table>
                );
        }
        else{
            return null;
        }
}

export default DrawTable; 